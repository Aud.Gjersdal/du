# Digital utvikling ved Universitetsbiblioteket i Bergen

# Systemoversikt og systemdrift ved fagruppe for digital utvikling ved Universitetsbiblioteket i Bergen

#### Begreper og roller:
* Systemeier: https://internkontroll-infosikkerhet.difi.no/begrepsliste-systemeier 
En systemeier er en leder som er ansvarlig for å utvikle, forvalte og drifte et informasjonssystem. UB ved avdelingsdirektør er systemeier for de fleste av systemene som er listet nedover. I noen tilfeller kan for eksempel fagmiljø være systemeier, mens UB er operativ systemeier/systemforvalter.
* Systemeier fellessystem: https://internkontroll-infosikkerhet.difi.no/begrepsliste-systemeier-fellessystem En systemeier fellessystem er ansvarlige for å utvikle, forvalte og drifte et informasjonssystem som benyttes av flere risikoeiere i virksomheten. 
* Systemforvalter: https://internkontroll-infosikkerhet.difi.no/begrepsliste-systemforvalter Den person systemeier har pekt ut som operativt ansvarlig for et informasjonssystem på vegne av systemeier. For faggruppe digital utvikling er hovedansvaret faglig leder i samarbeid med med seksjonsleder UFS. 
* Funksjonseier: Brukes her for den som eier selve funksjonen som tjenesten yter, for å skille fra systemeier. Dette er ikke nødvendigvis den samme som utvikler, drifter og forvalter systemet. Dette kan for eksempel være fagmiljø eller andre avdelinger ved UiB.
* Prosjekteier: Prosjekteieren er personen som blir utpekt som overordnet ansvarlig for at prosjektet når sine mål. 
* Risikoeier: Seksjonsleder UFS https://internkontroll-infosikkerhet.difi.no/begrepsliste-risikoeier
* ROS-ansvarlig: Per i dag faglig leder digital utvikling.
* Produkteier/produkteierteam: (ikke i bruk enda) Produkteierteam er kryssfunksjonelle ved at de består av relevante fagdisipliner for å realisere løsninger som dekker behovene og når målsettingene. Produktier leder et produkteierteam.

# Systemer:


## 1. MerMEId (ariel.uib)


 | URL    | System | Plattfprm | Språk | Host  | Funkjsonseier | Systemeier | Systemforvalter|
 | ------ | ------ | ----------|-------| ----- |-------------  | ---------- | -------- |
 | http://ariel.uib.no/editor/ | MerMEId | eXist | XML MEI, java, NoSQL | vmWare | KMD (GA) | UB | Øyvind |
 
MerMEId er et system for redigering, håndtering  og forhåndsvisning av musikk-metadata, basert på MEI (Music Encoding Initiative) XML-skjema.

#### Systemforvalter: 
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Lenker til Repo:
* https://github.com/ubbdst/MerMEId  
* https://gitlab.com/ubbdev/MerMEId (vår kildekode) 
* https://gitlab.com/ubbdev/MerMEId-backup (backup av data fra xml-database) 
* https://gitlab.com/ubbdev/merMEId_XML (bruker Mermeid-backup) 
* https://git.app.uib.no/grg/ansible-grg -> prototype konfigurering av server, ikke i bruk. 
* https://sak.uib.no/projects/py2/wiki -> Dokumentasjon fra installasjon  

#### Type system: 
* Open Source, utviklingspartner
* Funksjon: Redigering og vising av musikk-metadata, koblet til Grieg Research Guide (GRG)
* Kategori: Editor, metadata, musikk, forskning, XML (tekst), backend

#### Driftsløsning:
* 

#### Kompetanse DU:
* Øyvind Liland Gjesdal

#### Samarbeidspartnere/personer:  
*  Faggruppe forskningstøtte UB, Kirstine Folman, universitetsbibliotekar i musikk
*  Arnulf Mattes, Grieg Akademiet
*  Alex Geertinger, Kongelige Bibliotek Danmark (utvikler) 

#### Integrasjoner:
*  Har utviklet JSON API som GRG benytter.

#### Utfordringer:
*  MerMEId oppdateres ikke lengre? 
*  Konto for innlogging lages rett på server (trenger dokumentasjon)
*  Exist-DB forkaster tomcat i nyeste utgave av exist-db. Kanskje bidra/hjelpe MerMEId med flytt til en jetty-basert workflow? 

#### Løsningsforslag:
*  Beholde og redesigne
*  Planlegg oppgradering

#### Oppgaver:
*  Bygge ansible
*  Deploye til NREC
*  https
*  ExistDB -> Tatt vekk støtte for Tomcat, som er MerMEIds 
*  Bidra med kode til KB


## 2. Avhandlingsportalen (BYOTh + GrOIN) 

 | URL    | Frontend | Backend | Database | Språk | Host | Funksjonseier | Systemeier fellessystem | Systemforvalter | 
 | ------ | -------- | ------  | -------- | ----- | ---- | ------------- | ---------- | -------- | 
 | https://avhandling.uib.no | Angular | Express | Fedora,  Redis | javascript, typescript | Azure | UB/FiA | UB | Ahl | 

System for innlevering av doktorgradsavhandlinger fra UiB til trykking, lagring og åpen tilgjengeliggjøring, samt levering av pressemeldinger og kalenderoppføringer til w3 (uib.no).
Presentasjon: https://slides.com/simonmitternacht/byoth#/  

#### Systemforvalter: 
Faggruppe digital utvikling, Ahl Vegard Nilsen

#### Lenker til Repo:
https://git.app.uib.no/uib-ub/publiseringsstotte/phd-portal 

* GrOIN: 

#### Testinstans:
https://avhandling.test.uib.no/ 

#### Type system: 
* Open Source, egenutviklet 
* Funksjon: Innlevering, trykking og åpen tilgjengeliggjøring av doktorgardsavhandlinger, tilgjengeliggjøring av pressemedlinger og kalenderoppføringer.

#### Driftsløsning:
* 

#### Kompetanse DU:
* Ahl Vegard Nilsen
* Hemed Ali Al Ruwehy

#### Samarbeidspartnere/personer:  
* Cato Kolsås ITA (utvikler Besvarelsesrepo), ny gruppe/personer ved omorganisering ITA?
* Lennart Nordgreen ITA (skydrift)
* Faggruppe forskningstøtte UB, Irene Eikefjord, spesialbibliotekar

#### Staus:
*  Risikovurderet h-2020
*  Forvaltningsgruppe Besvarelsesrepo og Avhandlingsportalen til vår 2021 - ansvar for drift og videreutvikling. Forslag til videre utviklingsoppgaver leveres våren 2021. 
*  Administrasjonsgrensesnitt på tilknyttet database Besvarelsesrepo utviklet høst 2020 (ITA)
*  Egenutviklet apllikasjon for uthenting av ISBN (GrOIN)
*  Egenutviklet applikasjon for å hente ut status på innlevert endelig versjon (PDF/A) fra trykkeri
*  FS-integrasjon under utvikling

#### Integrasjoner:
*  W3 (uib.no)
*  BORA (UNIT)
*  Besvarelsesrepo (Fedora, database)
*  Trykkeri (impelo web)
*  SEBRA
*  Romdata UiB
*  GrOIN
*  Applikasjon for status avhandlinger lastet opp fra trykkeri

#### Utfordringer:
*  Egenkomponert løsning der viktige verktøy som errorlogging fra klienter og serverlogger først har kommet på plass etter hovedutvikler har sluttet.
*  Mange dependencies, må konstant oppdateres med patches
*  Integrasjon med Besvarerelsesrepo (Fedora) som database kan ha vært uheldig. UiB bør vurdere alternative lagringsløsninger for forvaltningsdata, som betyr omskriving av backend api med mindre man beholder sin egen Fedora-instans.

#### Løsningsforslag:
*  Vurdere alternativ database?
*  På sikt: Vurdere opp mot innleveringsløsninger for masteroppgaver (felles system)/ nasjonal løsning
*  Gjøre portal uavhengig av trykking
*  Mer stabil drift, egen kompetanse 

#### Oppgaver:
*  Følg opp ROS fra 2020
*  Ferdigutvikle integarsjon FS
*  Følge opp backburner jevnlig
*  ITA flytter driftoppsett til Amazon?
*  Mer stabil driftsløsning (ta større del ansvar selv?)
*  Behov for videreutvikling?
*  Følg med Utvikling Nasjonalt vitenarkiv UNIT

## 3. Re-assesing St. Birgitta of Sweden (Omeka-S)
 
 | URL  (test)  | System | Språk | Host  | Prosjekteier | Systemeier | systemforvalter | 
 | ------ | ------ |------ | ----- | ------------- | ---------- | -------- |
 | http://birgitta.test.uib.no/ | Omeka S/ https://omeka.org/s/ | php | NREC | Laura Miles, Fremmedspråk | UB?  | Ahl |
 
NFR-prosjektet Re-assesing St. Birgitta 2019-2023. UB bidrar til to arbeidspakker (2019-2021): 1. Bygge database, og 2. Editions Lab. Arbeidet består i å bygge en import fra skjema til infrastruktur, konfigurerer indekserings struktur, mappe til og utvide datamodell, visualisere data og laste opp billedfiler og editert tekst for hver innførsel ved bruk av IIIF og TEI.

#### Systemforvalter:  
Må avvklares når/hvis prosjektet går i drift, per i dag: Faggruppe digital utvikling, Ahl Vegard Nilsen

#### Lenker til Repo:
*  https://git.app.uib.no/uib-ub/ansible-omeka
*  https://git.app.uib.no/revision
*  https://git.app.uib.no/revision/revision-visualization

#### Testinstans: 
http://birgitta.test.uib.no/ 

#### Type system: 
*  Open Source, CMS
*  Funksjon: Visualisering av nettverksanalyser
*  Kategori: Samling en person, lenket data, tekst, bøker og manuskripter, mennesker og steder, bibliografi,  nettverk, analyse, prosjekt, forskning

#### Driftsløsning:

#### Kompetanse DU:
* Ahl Vegard Nilsen
* Hemed Ali Al Ruwehy
* Øyvind Liland Gjesdal?

#### Samarbeidspartnere/personer:
* Laura Miles, Fremmedspråk
* Julia King, Fremmedspråk

#### Status:
* Prototype, prosjekt
* Ansible i testmaskin

#### Utfordringer:
* UB bidrar i to arbeidspakker i NFR-prosjektet 
* Har en løsere tilnærming til domain og range i datamodeller, som gjør at mennesker kan bryte strengere datamodeller 
* Export av data til triplestore -> Sparql queries til Gephi -> OmekaS
* Egenutviklet visualiseringsapplikasjon (ved hjelp av tredjepartsbibliotek). Kunne vært mer generisk.

#### Fordeler:
*  Er basert på Linked data og kan dekke småskala prosjekt.
*  Multitennant, samme installasjon kan gjenbrukes av alle aktører dersom man velger et godt domene.

#### Løsningsforslag/oppgaver:
*  Avklare videre drift etter prosjektslutt
*  Kan Omeka brukes for andre samlinger og prosjekt ved UB? 


## 4. BOAP (Bergen Open Access Publishing)
 
 | URL    | System | Språk | Host  | Systemeier | Systemforvalter |   
 | ------ | ------ |------ | ----- | -----------| ----------------| 
 |  http://boap.uib.no | OJS | php | vmWAre | UB | Øyvind? |

System for publisering av åpne tidsskrift.
 
#### Systemforvalter: 
Fagruppe forskningstøtte UB, Øyvind Liland Gjesdal

#### Type system:
* Open Source applikasjon, utvikles av PKP
* Funksjon: Åpen tilgang til forkningspublikasjoner
* Kategori: Publisering, forskning, PDF

#### Driftsløsning:

#### Kompetanse DU:
*  Øyvind Liland Gjesdal
*  Ahl Vegard Nilsen
*  Tarje Sælen Lavik

#### Samarbeidspartnere/personer: 
* Faggruppe forskningstøtte UB, Tormod Eismann Strømme, spesialbibliotekar

#### Status:
*  Ansible test-prosjekt
*  ROS-analyse utført høst 2021

#### Integrasjoner:
*  Sender metadata og DOI til Crossref

#### Utfordringer:
*  En del bugs i OJS
*  Vi klarer ikke å oppdatere raskt nok

#### Løsningsforslag:
*  Vurdere nasjonal løsning for drift av tidsskrift og serier på sikt, alternativ løsning kan være drift av PKP

#### Oppgaver:
*  Følg opp ROS fra høst 2020
*  Oppgradere i løpet av 2021
*  Ad-hoc støtte til Tormod i PDF-generering
*  Undersøke mulighet for å outsource drift


## 5. CLARINO Repository
 
 | URL    | System | Host  | Funksjonseier/ prosjekteier | Systemeier | Systemforvalter |
 | ------ | ------ |------ | ------------- | ------     | ------   |
 |  http://repo.clarino.uib.no | DSpace | vmWare | LLE | UB | Hemed | 

Repository for språkdata. Gjøre eksisterende og fremtidig forskningsdata tilgjengelig for forskning.

Prosjektstart 12. april 2012 (se blogg https://clarin.w.uib.no/2012/04/). UB har siden 2012 ansvaret for repo.
Ny prosjektstart januar 2020 (Clarino+). UB fortsetter med samme ansvar og garanterer for langsiktig drift. Clarino repo, med webside som også er webside for bergenssenteret, er en del av UBs infrastrukturdrift for HF-fagene.

#### Systemforvalter:
Faggruppe digital utvikling, Hemed A. Al. Ruwehy

#### Type system: 
* Open source, utvikling Dspace-community og noe egenutvikling  
* Funksjon: Lagre og tilgjengeliggjøre språkdata åpent tilgjengelig
* Kategori: Forskningsdata, fagnært, frontend og backend

#### Driftsløsning:

#### Kompetanse DU:
* Hemed Ali Al Ruwehy
* Øyvind Liland Gjesdal
* Rune Kyrkjebø, førstebibliotekar, adm.
* Emma Josefin Ölander Aadland, universitetsbibliotekar nordisk, adm.

#### Samarbeidspartnere/personer: 
* Koenraad De Smedt, LLE
* LINDAT (Praha)
* Clarino+ prosjekt: UB UiT, NB, NHH, NSD

#### Status:
*  Risikovurdert h-2019
*  Clarino+ søknad (prosjekt 2020-2022) fra NFR. Arbeidspakker tilknyttet DU (Rune, Hemed), Paul i HF-arbeidstid.

#### Integrasjoner:
*  CLARIN Virtual Language Observatory, https://vlo.clarin.eu/? 

#### Utfordringer:
* Dspace drift 

#### Løsningsforslag:
* Fortsette å drifte CLARINO med support av Clarin DSpace community som nå.
* Med tiden vurdere annen teknisk løsning. Se i sammenheng med UiB Open research Data: https://dataverse.no/dataverse/uib 

#### Oppgaver:
* Flytte til ny server høst 2020 (eol Redhat 6)
* Arbeid knyttet til Clarino+ prosjektet
* Ansible-oppsett ved oppgradering?

## 6. Digitalt

 | URL    | System | Språk |  Host  | Systemeier | Systemforvalter |   
 | ------ | ------ |------ | ------ | ------------ | ---------- |  
 |  https://digitalt.uib.no | DSpace | java | NREC  | UB | Øyvind |   

Åpent arkiv med samlinger fra Spesialsamlingene (The Mahmoud Salih Collection, gamle bøker, The Norwegian Institute at Athens, Krigstrykk etc).

#### Systemforvalter: 
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Type system: 
* Open source, utvikles av DSapce-community.
* Funksjon: Åpen tilgang til spesilasamlingsmateriale (pre Marcus) og materiale som ikke passet inn i BORA pga forfattertilhørighet utenfor UiB.
* Kategori: Samlinger, manuskripter, PDF, frontend og backend 

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal
* Hemed Ali Al Ruwehy

#### Løsningsforslag:
Innhold flyttes og system avvikles. Noe innhold allererde flyttet til BORA. Midlertidig DSpace satt opp høst 2020. Avvikles og innhold flyttes til Marcus våren 2021.

#### Oppgaver:
*  Digitalt avvikles våren 2021. 


## 7. Grieg Reserach Guide (GRG)
 
 
 | URL    | Frontend | Backend | Database | Språk | Host  | Tjenesteieier | Systemeier |  Systemforvalter |
 | ------ | -------- |-------- | -------- | ------ |----- | ------------- | ---------  | -------- |
 | http://grg.uib.no/home  | Angular | Loopback3 | MongoDB | ? | vmWare | KMD (GA) | UB | Ahl |

Grieg Research Guide er en nettbasert guide inneholdende en kommentert bibliografi over litteratur om Grieg og en verkfortegnelse over Griegs verk med semantiske relasjoner mellom referanser, personer og verk.
 
* Grieg Research Guide er en webapplikasjon som lagrer artikler I en MongoDB via Loopback3 rammeverket. 
* Disse artiklerne lenker til referanser som importeres fra Zotero og verk fra MerMEId. 
* Koden ble forsøkt gjenbrukt I NorLaw, men innsatsen for å sette det opp og gjøre tilpassninger ble ansett for stor I forhold til merverdien utover å bare bruke Zoteros egne funksjoner 

#### Systemforvalter:
Faggruppe digital utvikling, Ahl Vegard Nilsen

#### Lenker Repo:
https://gitlab.com/ubbdev/grg-api  
https://gitlab.com/ubbdev/grg-frontend   
 
#### Type system: 
* Open Source, egneutviklet
* Funksjon: Åpen tilgang til kommentert biografi over litteratur og musikalske verk for forskning.
* Kategori: Samling en person, bibliografi, metadata, lydfiler, nettverk, lenket data, tekst, forskning, frontend og backend

#### Driftsløsning:

#### Samarbeidspartnere/personer: 
* Faggruppe forskningstøtte UB, Kirstine Folman, universitetsbibliotekar musikk 
* Arnulf Mattes, Grieg Akademiet
* Arvid Vollsnes, pensjonist
* Robin Garen Aaberg, Knowit, utvikler, robin.garen@gmail.com, med som privatperson 

#### Integrasjoner:
* MerMEId 
* Zotero 
* Dataporten

#### Utfordringer:
* Egenkomponert CMS med alle de sikkerhets- og oppdateringsproblemene det medfører 
* Mye kode for lite funksjonalitet 
* Editor som krever for mye når nye behov melder seg 

#### Løsningsforslag:
*  Headless CMS og static site generator, f.eks. Sanity.io og Gatsby.js
*  Omeka S? (Se Birgitta-prosjektet)
*  Samtaler med Robin indikerer at det kan komme mer avanserte funksjoner på toppen av MerMEId, de kan kanskje integreres i GRG (i ny versjon?) 

### Oppgaver:
*  Flytte til NREC?
*  Bygge Ansible?
*  Noe utviklingsoppgaver gjenstår etter prosjektslutt
*  Møte med Kirstine og Arnulf for oppfølging og videreutvikling 


## 8. Ludvig Holbergs skrifter
 
 | URL    | System | Host  | Funksjonseier | Systemeier | Systemforvalter |
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | http://holbergsskrifter.no | XTF | NREC | UiB, Det Danske Sprog- og Litteraturselskab | UB | Øyvind |

I denne tekstkritiske utgaven gjøres Holbergs samlede forfatterskap digitalt tilgjengelig. Tekstene er søkbare og lenket opp mot faksimiler av førsteutgavene, og er utstyrt med ord- og sakkommentarer, innledninger og tekstkritiske noter.

#### Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Lenker Repo:
* https://git.app.uib.no/lhs/sys/ansible-lhs
* https://github.com/cdlib/xtf 
* https://git.app.uib.no/lhs/sys/xtf-lhs (XTF fra Øystein Reigem, videreutviklet ved UB)
* https://git.app.uib.no/lhs/tekster-lhs (TEI-filer)
* https://git.app.uib.no/lhs/schema-lhs (TEI-skjema)
* https://git.app.uib.no/lhs/ressurser-lhs (eksterne ressurser som pdf som skal serves fra siden)

#### Type system:
* Egenutviklet 
* Funksjon: Åpen tilgang til Ludvig Holbergs forfatterskap.
* Kategori: Smaling en person, bibliografi? tekster, forskning, 

#### Driftsløsning:

#### Integrasjoner:
Bruker bildeserver fra kb.dk for faksimiler.

#### Kompetanse DU:
*  Øyvind Liland Gjesdal
*  Tone Merete Bruvik

#### Samarbeidspartnere/personer: 
* DSL, Eiliv Vinje, LLE

#### Status:
*  Git support, men ingen utvikling

#### Oppgaver:
*  https?
*  Overføre bilder fra ub.uib.no, hoste billedfiler i IIIF?

## 9. Hordanamn

| URL    | System | Host  | Funkjsonseier | Systemeier | Systemforvalter|  
| ------ | ------ |------ | ------------- | ---------- | -----------------|
| http://hordanamn.uib.no/lydkart/ | eXist-db/Python | NREC | LLE | UB | Øyvind | 

Stadnamn i hordaland innsamlet av  Ole-Jørgen Johannessen. Bruker kan søke på stedsnavn i Hordaland,
se navnene i kart, og høra den lokale uttalen.

#### Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Type system: 
*  Egenutviklet 
*  Funksjon: Tilgang til data om stedsnavn
*  Kategori: Ord (data), tekst, språk

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal

#### Samarbeidspartnere/personer
* Peder Gammeltoft, faglig leder, Faggruppe språksamlingene
* Kurdin Jacob, seniorkonsulent, Faggruppe språksamlingene UB?
* Ole-Jørgen Johannessen, pensjonist LLE

#### Lenker Repo:
* https://git.app.uib.no/hordanamn/ansible-hordanamn (Inneholder kun info fra oppsett, første forsøk på ansible, ble ikke til en kjørende konfigurering)
* https://git.app.uib.no/hordanamn/tools-hordanamn (Øystein sine batch verktøy for å laste inn nye stedsnavns-data).
* https://git.app.uib.no/hordanamn/html-hordanamn (tre applikasjoner kjørt mot hordanamn data) 
* https://git.app.uib.no/hordanamn/cgi-hordanamn (tjenester for å å kjøre python-transformasjon m.m

For oversiktsdokumentasjon se repo cgi-hordanamn.

#### Status/oppgaver:
*  NREC
*  Første forsøk på ansible, satt opp manuelt


## 10. Marcus
 
 | URL    | Frontend | Backend | Editor | Host  | Systemeier | Systemforvalter |
 | ------ | -------- |-------- | -----  |------ | -----------| -----------------|
 | http://marcus.uib.no/home | LODspeakr. | Fuseki | Protégé | VmWare/ NREC | UB | Hemed | 

Marcus er Spesialsamlingene til Universitetsbiblioteket i Bergen sin portal til digitaliserte manuskript, fotografi, diplomer og mye mer.

#### Systemforvalter:
Faggruppe digital utvikling, Hemed Ali Al Ruwehy

##### Lenker Repo:
* https://github.com/alangrafu/lodspeakr (utgangspunkt lodspeakr)
* https://bitbucket.org/ubbdst/marcus-special (Kildekode for marcus.uib.no) (@todo flytt til git.app ved neste videreutvikling) (Lodspeakr med egenutvikling)
* https://git.app.uib.no/uib-ub/ingen-lfs-data-marcus (data lagret fra Protege)
* https://bitbucket.org/ubbdst/blackbox (mellomlag elasticsearch og søkeklient)
* https://git.app.uib.no/uib-ub/elasticsearch-scripts (logikk av elasticsearch)
* https://github.com/ubbdst/elasticsearch-rdf-river (tillegg plugin ES for å konfiugerere indeksering av RDF) 
* https://github.com/ubbdst/protege-owl-plugin (Våre endringer til Protege)

#### Type system:
* Open source, egenutviklet
* Funksjon: Åpen tilgang til og formidling av spesialsamlinger (manuskript, fotografi, diplom, utsillinger).
* Kategori: Samling/arkiv, manuskript, fotografi, billedfiler (IIIF), transkripsjoner, lenket data, formidling, forskning, UB sine egne samlinger, frontend og backend

#### Driftsløsning:

#### Kompetanse DU:
* Hemed Ali Al Ruwehy
* Øyvind Liland Gjesdal
* Tarje Sælen Lavik
* Ahl Vegard Nilsen (under opplæring)
* Robert Kristof Paulsen (under opplæring)
* Rui Wang (under opplæring)

##### Samarbeidspartnere/personer:
* Billedsamlingen UB, Morten Heiselberg?, faglig leder
* Manuskript- og librarsamlingen UB, Alexandros Tsakos, faglig leder
* Marianne Paasche, digital arkivar, Manuskript- og librarsamlingen

#### Status:
*  ROS-analyse utført høst 2019
*  Videre utvikling evt. vurderer nytt system/ nye deler
*  Sees i sammenheng med videreutvikling i Samla-prosjektet

#### Utfordringer: 
* Må ha egen/sentralisert løsning for distribuering, redigering og lagring av bildefiler og IIIF manifest 
* IIIF Image API for distribuering av enkelt filer 
* Kan ha statisk frontend, da dette kan forenkle og gjøre det billigere å distribuere nettsider, men usikkert hvordan oppdatering vil kunne skje. Må alle sider regenereres hver gang? 

#### Løsningsforslag: 
* Videreutvikle dagens løsning sett i sammenheng med utviklingsoppgaver i SAMLA-prosjektet (editor, tilgangskontroll, søk og webgrensesnitt) og eventuelt med prosjekt eLAM
* Dersom man etter hvert går for ekstern løsning, kan man fremdeles lage webpresentasjon, ala nettustillinger i en egen løsning med integrasjoner til ekstern
* Integrasjon mot arkivkatalog, Aasta? Avvente hva som skjer i Arkivverket på dette området.

#### Oppgaver:
*  Løse konkrete oppgaver og behov fra Spesialsamlingene
*  Vurdere videre utvikling i forbindelse med Samla-prosjeketet og formildingsprosjekt (eventuelt ogsp eLAM). Teste løsninger for filoverføring fra digitalisering (Goobi mfl.), editor (Web Protégé mfl.), søk, transkripsjoner, formidling etc.
*  Følge opp ROS med tiltak, blant annet for lagring av billedfiler (BILLY) 
*  Sees i sammenheng med prosjektene SAMLA og eLAM, formidlingsprosjekt

## 11. Medieval Linked Open DATA (MeLOD)
 
 | URL    | Frontend | Backend | Host  | Prosjekteier | Systemeier | Systemforvalter|
 | ------ | -------- |-------- | ----- |-------------- | ---------- | -------  |
 | https://melod.uib.no | LODspeakr. | Fuseki, Elastcsearch | NREC | Åslaug Ommundsen, LLE | UB | Øyvind | 

Lenking av middelalderske datasett. Åpen tilgang til fragmenter og manuskripter.

#### Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Samarbeidspartnere/ personer:
* Åslaug Ommundsen, LLE
* Gjert Kristoffersen, Målføresamlinga

#### Type system:
* Open source, egenutviklet (samme plattform som Marcus)
* Funksjon: Åpen tilgang og lenking av middelalderske datasett (fragmenter og manuskript).
* Kategori: Manuskript, billedfiler (IIIF), lenket data, forskning, prosjekt, frontend og backend

#### Driftsløsning:

#### Kompetanse DU:
* Hemed Ali Al Ruwehy
* Øyvind Liland Gjesdal
* Tarje Sælen Lavik

#### Status:
*  Marcus i v5
*  Aldri beveget seg ut over prosjektfase

#### Utfordringer /løsningsforslag:
*  Se kommentaerer under Marcus, må sees i sammenheng med hva som skjer der
*  Målføresamlinga akulle inn i Marcus-v5, men nå lever datasettet uten hjem 

### Oppgaver:
*  Se i sammenheng med videreutvikling av Marcus 
*  Plan for videreutvikling. Integrasjon med Marcus?

## 12. Menota CMS
 
 | URL    | System | Språk | Host  | Funksjonseier | Systemeier | Produktansvarlig | 
 | ------ | ------ |------ | ------------- | ------------ | ------ | ------ |
 | https://menota.org/forside.xhtml |  Egenutviklet (Corpuscle) | TEI (XML), XSLT | NREC | LLE | UB | Øyvind | 

Ligger under Clarino. Arkiv for Nordiske middeladertekster. Et nordisk nettverkssamarbeid der UB Bergen er teknisk base. Menota har langtids driftsavtale med UB . Avtale inkluderer drift, feilrettinger, mindre endringer (XML og XSLT).

####  Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Lenke til Repo:
https://git.app.uib.no/Menota 

#### Type system:
* Open source, egenutviklet
* Funksjon: Åpen tilgang til middelalderske tekster, oversettelser, transkripsjoner
* Kategori: Tekst, xml, transkripsjon, språk, forskning

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal
* Tone Merete Bruvik
* Robert Kristof Paulsen

#### Samarbeidspartnere/personer:
* Odd Einar Haugen, LLE

#### Oppgaver:
*  Ansible, NREC
*  Endringsønsker CMS cirka < 5 ganger per år
*  Noe generering av word-filer fra xml
*  Utviklingsoppgaver?

## 13. Menota Korpus

 | URL    | System | Host  | Funkjsonseier | Systemeier | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------     | ------   |
 | http://clarino.uib.no/menota.org | Egenutviklet (Corpuscle) | INESS-klyngen | LLE | UB | Paul |

#### Repo:
* SVN, Git?

#### Systemforvalter:
Faggruppe digital utvikling, Paul Meurer 

#### Kompetanse DU:
* Paul Meurer

#### Samarbeidspartnere/personer:
* Odd Einar Haugen, LLE

## 14. Proxy-tjeneste

| URL    | Programvare | Host  | Systemeier | Systemforvalter |  
| ------ | ------ |------ | ------------- | ------     | 
 | http://pva.uib.no | ezproxy: http://www.oclc.org/ezproxy | ? | UB | Ahl |

Tjenesten, som krever pålogging, gjør det mulig å få tilgang til UBs lisensierte e-ressurser fra utenfor UiBs nettet for ansatte og studenter ved UiB. 
Bibliotekets hjemmeside er satt opp slik at tjenesten automatisk kalles opp når det er nødvendig for tilgang til e-ressursene utenfor campus. 

#### Systemforvalter: 
Faggruppe digital utvikling, Ahl Vegard Nilsen

#### Samarbeidspartnere/personer:
* Faggruppe system, Ellen Solvik, faglig leder system
* Adriana Morys-Fornal?, spesialbibliotekar system 

#### Utfordringer:
* Proxy-server er i dag satt opp til å spille sammen med programmer fra Exlibris (Primo, Metalib og SFX) i Uniportsamarbeidet.
* Vi må sette opp nytt biblioteksystem fra OCLC, slik at det mest mulig sømløst kan benytte proxy-server, om dette vil kreve endringer på proxy-server må avklares.

#### Løsningsforslag/oppgaver:
* Det kommer nye release av software. Det er ikke nødvendig eller ønskelig løpende å holde seg til siste versjon, 
men det kan være nødvendig å oppgradere, dersom vi trenger funksjoner/muligheter som kun finnes i nyere  utgaver.
* Systemansvar flyttes til ToS og så kan helle utvikler DU være knyttet til dette arbeidet?


## 15. Skeivt arkiv formidling (Drupal)

| URL    | System | Host  | Systemeier | Systemforvalter |     
| ------ | ------ |------ | -----------| ---------------- | 
| https://skeivtarkiv.no/ | Drupal | NREC? | UB | Tarje? |   
 
Formidling av skeivt arkiv, skeivopedia, fortellinger. Skeivt arkiv tar vare på, dokumenterer og formidler skeiv historie eller LHBT-historie.

#### Systemforvalter: 
Faggruppe Digital utvikling, Traje Sælen Lavik?

#### Samarbeidspartnere/personer:
* Skeivt Arkiv, Hannah Gillow Kloster, faglig leder
* Digital arkivar, Skeivt Arkiv

#### Type system:
*  Open Source CMS levert av ITA
*  Funksjon: Formidling av Skeivt Arkiv
*  Kategori: Arkiv/samling, manuskripter, fotografi, video, ord, billedfiler (IIIF), lenket data, formidling og forskning, frontend

#### Driftsløsning:

#### Løsningsforslag:
*  Headless CMS? 
*  Sees i sammenheng med vurdering av CMS for andre systemer 

#### Status:
* Mangler kompetanse på Drupal
* Ingen videreutvikling 

#### Oppgaver:
*  Løse konkrete behov og vurdere utviklingsoppgaver
##

## 16. Skeivt arkiv katalog (Marcus-plattform)

 | URL    | Frontend | Backend | Editor | Host  | Systemeier | Systemforvalter | 
 | ------ | -------- |-------- | -----  |------ | ------------- | ---------- | 
 | http://katalog.skeivtarkiv.no/collections  | LODspeakr | Fuseki | Protégé | ? | UB | Hemed | 


#### Systemforvalter:
Faggruppe digital utvikling, Hemed Ali Al Ruwehy

#### Status:
*  Arbeid på katalogen er neglisjert. Ikke oppdatere skjema i Protégé. Planen var at SkA skulle bruke samme system som Marcus.

#### Løsningsforslag/oppgaver:
*  Se i sammenheng med utvikling Marcus
*  Vurdere å implementere Arkivportalen (1 ukes arbeid?). Må vurderes opp mot planer i Arkivverket.

#### Type system:
*  Open source, egenutviklet
*  Funksjon: Åpen tilgang til/formidling til skeivt arkivmateriale
*  Kategori: Manuskript, billedfiler (IIIF), lenket data, forskning og formidling, frontend og backend

#### Driftsløsning:

## 17. Stadnamn

 | URL    | Frontend | Backend | Editor | Host  | Systemeier | Systemforvalter |
 | ------ | -------  |-------- | -------| ----- | -----------| ---------------- | 
 | https://toponymi.spraksamlingane.no/ | Sampo-UI/Trifid | Jena Fuseki (triplestore), IIPimage (bildeserver) | ingen editor  |  NREC  | UB  | Øyvind   |

* Sampo-UI: https://github.com/SemanticComputing/sampo-ui (frontend)
* Trifid: https://github.com/zazuko/trifid (frontend)
* Jena Fuseki: https://jena.apache.org/ (backend, triplestore)
* IIPimage: https://iipimage.sourceforge.io/ (backend, bildeserver) 


Søk, analyser og visualiser stedsnavndata.

Vi gjer òg bruk av disse teknologiane
Lucene for søk (sjå her for søkemoglegheiter)
IIPImage som bileteserver
Jena Fuseki som triplestore SPARQL server
Lipvips for biletebehandling
Trifid Zazuko som LOD/lenkede data server
NREC til hosting av portalen

##### Lenker til repo:
https://git.app.uib.no/spraksamlingane/stadnamn

#### Operativ systemeier:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Type system:
*  Linked Data, RDF
*  IIIF bildeserver og presentasjon

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Lilad Gjesdal

#### Samarbeidspartnere/personer:
* Faggruppe Språksamlingene, Peder Gammeltoft, faglig leder
* Kurdin Jacob, seniorkonsulent, faggruppe språksamlingene

#### Status:
*  Prototype

#### Oppgaver:
*  Etablere plattform


## 18. Søk & Skriv

 | URL    | System | Host  | Funksjonseier | Systemeier fellessystem | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------------------------| ---------   | 
 | http://sokogskriv.no/  | VuePress | radisson7 ? | Søk & Skriv | UB  | Tarje |
 
Søk & Skriv er laget for studenter som ønsker å lære mer om informasjonssøk, studieteknikk og akademisk skriving.  

#### Lenke til repo:
https://git.app.uib.no/uib-ub/sok-og-skriv |
 
#### Systemforvalter:
Faggruppe digital utvikling, Tarje Sælen Lavik

#### Samarbeidspartnere/personer:
* Fagruppe utdanningstøtte UB, Ingunn Rødland, Søk & Skriv redaksjon (UB)
* Ingerid Straume, UiO, Søk & Skriv redaksjon (leder)

#### Type system:
*  Open Source, static site generator CMS
*  Funksjon: Utvikling (innhold) og tilgang til læringsressurser
*  Kategori: Tekst, video, læringsressurser, egenprodusert innhold, fellessystem på tvers av UH-bibliotek, CMS

#### Driftsløsning:

#### Status:
*  Byttet til ny plattform våren 2020

#### Oppgaver:
*  Noe mindre utviklingsoppgaver og vedlikehold
*  Avvikle Søk & Skriv på Wordpress

## 19. Termwiki:

 | URL    | System | Host  | Systemeier fellessystem | Systemforvalter |   
 | ------ | ------ |------ | ------------------------| ------   |
 | http://www.termwiki.sprakradet.no./wiki/Hovedside | Mediawiki | ? | ? | Øyvind |
 
Søk på norske og engelske fagtermer. 

#### Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal 

#### Type system:
* Open source (wiki), noe egenutvikling
* Funksjon: Søk og tilgang til fagtermer på norsk og engelsk.
* Kategori: Ord, språk, forskning

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal

#### Samarbeidspartnere/personer:
* Faggruppe språksamlingene UB, Peder Gammeltoft faglig leder
* Sindre Sørensen, sindre@sedb.no, Terminologi, Mediawiki med tilpassinger 
* Gisle Andersen, NHH, “Eier” deler av terminologi-prosjektet 
* UB har som del av sitt Clarino-engasjement, i samarbeid med Språksamlingene (UB og IT-avd), arbeidet med å etablere terminologi.no med ny søkeside og nytt redigeringsgrensesnitt. 

#### Status:
*  Utvikling i Clarino+ prosjektet
*  Midler til ny utvikler via Språksamlingene

#### Oppgaver:
*  Oversette terminiologi.no til semantic mediawiki
*  Konvertering av data til wikikode
*  Ansible oppsett
*  Bestille domene


## 20. Wittgenstein Archives (WAB)
WAB har langtids driftavtale med UB. Avtale inkluderer drift, feilrettinger, mindre endringer (XML og XSLT).


### 20a. WAB - Wittgenstein Ontology Explorer- Nachlass semantics
 
 | URL    | System | Host  | Funkjsonsseier | Systemeier | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | http://wab.uib.no/sfb/ | XML, Elasticsearch| vmWAre | Filosofisk institutt | UB | Hemed |
 
Wittgenstein Ontology Explorer er ontologi-dreven søk i Wittgenstein metadata. Teknologi: Elasticsearch, Apache Fuseki, AngularJS og innebygget Elasticsearch bridge, Blackbox.

#### Systemforvalter: 
Fagruppe digital utvikling, Hemed Ali Al Ruwehy

#### Type system:
*  Egenutviklet

#### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal
* Hemed Ali Al Ruwehy

#### Samarbeidspartnere/personer:
* Alois Pichler, Frederic Kettelhoit, LLE
* Hans Biesenbach, the Brenner Archives at the University of Innsbruck 
* University of Salento, P. Sraffa


### 20b. WAB - Wittgenstein Archives (WS)- Nachlass transcriptions
 
| URL    | System | Host  | Funksjonsseier | Systemeier | Systemforvalter |  
 | ------ | ------ |------ | ------------- | ------     | ------   |
 | http://wab.uib.no/transform/wab.php?modus=opsjoner | XML/ Elasticsearch | NREC? | Filosofisk institutt | UB | Øyvind | 

#### Systemforvalter:
Faggruppe digital utvikling, Øyvind Liland Gjesdal

#### Type system:
*  Egenutviklet

### Driftsløsning:

#### Kompetanse DU:
* Øyvind Liland Gjesdal
* Hemed Ali Al Ruwehy

#### Samarbeidspartnere/personer:
* Alois Pichler, Frederic Kettelhoit, LLE. 

#### Løsningsforslag
*  Import av XML, generering av bilder
*  Hoste billedfiler i IIIF?

#### Oppgaver:
*  Flytte til ny server v-2020
*  Feilrettinger og mindre endringer


## 21. COMEDI

 | URL    | System | Host  | Språk | Funksjonseier | Systemeier | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | http://clarino.uib.no/comedi | Egenutviklet | INESS-klynge? NREC? | Common Lisp, SQL, Javascript | LLE? | UB | Paul |
 
 Clarin CMDI-1.2 metadata-editor. En del av Clarino-prosjektet.

 #### Repo:
 * SVN

#### Systemforvalter: 
Faggruppe digital utvikling, Paul Meurer

#### Driftsløsning:

#### Kompetanse DU:
* Paul Meurer

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i NREC
*  Videreutvikling i Clarino+


## 22. INESS

| URL    | System | Host  | Språk | Funksjonsseier | Systemeier | Systemforvalter |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | http://clarino.uib.no/iness | Egenutviklet | INESS-klynge? | Common Lisp, SQL, Javascript | LLE? | UB |Paul |
 

Infrastruktur for utforskning av syntaks og semantikk. En del av Clarino+-prosjektet.

#### Repo:
SVN

#### Systemforvalter:
Faggruppe digital utvikling, Paul Meurer

#### Driftsløsning:

##### Kompetanse DU:
* Paul Meurer

####Samarbeidspartnere/personer:
* Sindre Sørensen

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i NREC
*  Videreutvikling i Clarino+


## 23. Korpuskel/ Corpuscle

 | URL    | System | Host  | Språk | Funksjonsseier | Systemeier | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | http://clarino.uib.no/korpuskel | Egenutviklet | INESS-klynge? NREC? | Common Lisp, SQL, Javascript | LLE? | UB | Paul | 
 
 Korpusverktøy. En del av Clarino-prosjektet.

#### Repo:
SVN

#### Driftsløsning:

#### Systemforvalter:
Fagguppe digital utvikling, Paul Meurer

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i NREC
*  Videreutvikling i Clarino+


## 24. Redigeringsgrensesnitt revisjonsprosjektet

 | URL    | System | Host  | Systemeier | Systemforvalter | 
 | ------ | ------ |------ | ------------- | ------| 
 | http://clarino.uib.no/lex | Egenutviklet | INESS-klynge; Oracle | UB | Paul |

#### Repo:
SVN

#### Systemforvalter: 
Faggruppe Språpksamlingene, Paul Meurer

#### Type system:

#### Driftsløsning:

#### Kompetanse DU:
* Paul Meurer 

#### Samarbeidspartnere/personer:
* Faggruppe språksamlingene, Peder Gammeltoft faglig leder



## 25. Ordbokapp

| URL    | System | Host  | Systemeier fellessystem |  | 
 | ------ | ------ |------ | ------------- | ------| 
 |https://www.ordnett.no/ |  | | UB | Paul |


#### Type system:

#### Driftsløning:

#### Kompetanse DU:
* Paul Meurer

#### Samarbeidspartnere/personer:
* Faggruppe språksamlingene, Peder Gammeltoft faglig leder
* ITA

#### Operativ systemeier:
* Faggruppe språksamlingene, ?

 
## 26. Termportalen:

| URL    | System | Host  | Funksjonseier | Systemeier | Systemforvalter |
 | ------ | ------ |------ | ------------- | ------     | ------   |
 | https://oda.uib.no/app/term/aterm  | ? | ? | Språkrådet? | UB | ? |
 
Termportalen ved Språksamlingene er en nasjonal portal for terminologi.Termportalen er en fritt tilgjengeleg ressurs til norsk terminologi innenfor en lang rekke fagområder, der det er muglig å søkepå tvers av termbaser og fag.

#### Driftsløsning:

#### Type system:

#### Kompetanse DU:
* Paul Meurer

#### Samarbeidspartnere/personer:
* Språkrådet, NHH, UiO
* Del av Clarino+-prosjektet

# Prosjekt/utvilingsgrupper 
DU er/kan bli involvert som koordinator/prosjektleder/deltaker eller utviklerpartner i utvikling av tjeneste/digital infrastruktur.

## 1. Lagring & LOR
Prosjektet Lagring og LOR, under Læringslab-programmet tar sikte på å etablere fellesløsninger for å gi bedre arbeidsflyt og bedre dekning av behovene for lagring, deling og gjenbruk av digitale læringsressurser og video ved UiB. I hovedsak er dette tenkt løst ved å anskaffe og tilby et LOR-system som fungerer som et digialt bibliotek for læringsobjekter, samt en videolagringsløsning (MAM).

##### Prosjekteier:
* Mathilde Holm

#### Prosjektleder:
Gry Ane Vikanes Lavik, faggruppe digital utvikling

#### Prosjektgruppe:

#### Prosjekttilhørighet:
UiB Læringslab

### Systemer:
* Kaltura
* DLR


## 2. Clarino+:
NFR-infrastrukturprosjekt. Samarbeid med UiT,

#### Prosjekteier:

#### Prosjektleder:
Konrad de Smeedt, LLE

#### Prosjektgruppe UB:
* Rune Kyrkjebø, delprosjektleder/koordinator UB
* Øyvind Liland Gjesdal
* Hemed A. Al. Ruwehy
* Paul Meurer
* Emma Josefin Ölander Aadland

#### Utviklingsoppgaver:


## 3. SAMLA:
NFR-infrastrukturprosjekt. Samarbeid med 

#### Status:

#### Prosjektleder:
* Hans-Jacob Ågotnes, AHKR
* Prosjektkoordinator under ansettelse

#### Prosjektgruppe UB:
* Ingrid Cutler, delprosjektleder UB/koordinator
* Hemed A. Al. Ruwehy
* Robert Kristof Paulsen
* Ahl Vegard Nilsen
* Marianne Paasche, digital arkivar, Manuskript- og librarsamlingene
* Ny utvikler


#### Utviklingsoppgaver:


## 4. Formidlingsprosjekt Nansen:

#### Prosjekteier:

#### Prosjektgruppe:
* Gina Dahl, førstebibliotekar/arkivar Manuskript- og librarsamlingen
* Tarje Sælen Lavik, utvikler faggruppe digital utvikling
* Marianne Paasche, digital arkivar Manuskript- og librarsamlingen
* 

#### Utviklingsoppgaver:


## 5. Brukerstyrt innlevering:
?

## 6. eLAM (prosjektsøknad):
NFR-infrastrukturprosjekt. Samarbeid med Nasjonalbiblioteket, Arkivverket og UiO.

### Status:
Søknad sendt inn i november 2020. Avventer tilbakemedling NFR.

#### Arbeidsgruppe UB:
* Karin Cecilie Rydving, seksjonsleder UFS, Governance
* Tarje Sælen Lavik, utvikler faggruppe digital utvikling
* Ahl Vegard Nilsen, utvikler faggruppe digital utvikling
* Peder Gammeltoft, faglig leder språksamlingene
* Ingrid Cutler, faglig leder digital utvikling
* Emma Josefin Ölander Aadland, universitetsbibliotekar faggruppe digital utvikling, leder arbeidspakke Digital Scholarship Lab
* Nils Kristian Eikeland, digital arkivar, Billedesamlingen

## Digital Lab 
Formål å 

#### Utviklingsgruppe:
* Emma Josefin Ölander Aadland, universitetsbibliotekar faggruppe digital utvikling, koordinator
* Nils Kristian Eikeland, digital arkivar, Billedsamlingen
* Ahl Vegard Nilsen, utvikler faggruppe digital utvikling
* John-Eilhelm Flatun, universitetsbibliotekar, faggruppe undervisningstøtte
* Jenny Ostrup, universitetsbibliotekar, faggruppe forskningsstøtte

#### Utviklingsoppgaver:

# System der DU ikke er systemeier eller systemforvalter:

## 1. Norlaw (Norwegian Law in Foreign Languages)

 | URL    | System | Host  | Systemeier | Produktansvarlig | 
 | ------ | ------ |------ | ------------| ------     |
 | https://www.zotero.org/groups/1881030/norwegian_law_in_foreign_languages | Zotero | Zotero | UB | ? | 

Bibliografi norsk lov i fremmedspråk, editert av det Juridiske fakultetsbibliotek, UB

#### Operativ systemeier: 
Faggruppe undervisningstøtte, Elen Elvebakk

#### Kompetanse DU:
* Øyvind Liland Gjesdal

#### Type system:
Open source system for referansehåndtering.


## 2. Oria-integrasjon
| URL    | System | Host  | Systemeier | Produktansvarlig |  
|--------|--------|-------|------------|------------------| 
|  |  |  |  | ? | 
 
 #### Kompetanse DU: 
 * Ahl Vegard Nilsen
 * Øyvind Liland Gjesdal

#### Utfordringer/løsningsforslag:
 * Gjelder også indre utviklingsoppgaver/vedlikehold opp mot andre bibliotekssytem: Alma, Leganto?
 * Burde være på ToS? Eventuelt gruppe for samarbeid digital utvikling og ToS?

## 3. Aviskorpus:
| URL    | System | Host  | Systemeier | Produktansvarlig |   
| ------ | ------ |------ | ------------- | ------     |    
|  |  |  |  | ? |   

Er denne hos oss nå?

#### Kompetanse DU:
* Paul Meurer

## 4. Fragmentsamling:
| URL    | System | Host  | Systemeier | Produktansvarlig | 
 | ------ | ------ |------ | ------------- | ------     |
 |  |  | GitHub pages? | Åslaug Ommundsen  | Øyvind? | 

#### Kompetanse DU:
* Øyvind Liland Gjesdal

## 5. ELMCIP

| URL    | System | Host  | ¨Funkjsonseier | Systemeier | Produktansvarlig |  
| ------ | ------ |------ | -------------| ---------- | --------  |   
| https://elmcip.net | Durpral | ? | LLE | LLE | Stein Magne Bjørklund |  
 
#### Operativ systemeier: 
LLE

#### Kompetanse DU:
?

#### Samarbeidspersoner/personer:
Scott Rettberg, Stein Magne Bjørklund, Hannah Ackermans (LLE) 

#### Status UB:
*  Pilot
*  Med i Clarino+ prosjektet
*  UB har ikke noe systemansvar eller utviklingsoppgaver knyttet til ELMCIP per idag, men kan bli aktuelt å overta som systemeier.


#### Oppgaver:
*  Det er gjennomført et lite prosjekt som handler om hvordan UB kan bidra i metadataproduksjon ved å kvalitetsforbedre data. Det blir i November sendt en søknad til ledergruppen om at ToS bruker en bibliotekar en time i uken til å kvalitetsforbedre metadta.
*  Fremtidlig utvikling: Autoriteskontroll, overføring til Oria 
* Se formidlingsplattform i sammenheng med utvikling av CMS for andre tjenester

#### Type system:
*  Open source, CMS
*  Funskjon:
*  Kategori:

## 6. WAB - Nachlass search (wittgensteinarkivet)
 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Produktansvarlig | 
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | http://wittfind.cis.uni-muenchen.de/ |  | ? | ? | ? |  | 
 
#### Samarbeidspartnere/personer:
Alois Pichler, Frederic Kettelhoit, LLE


## 7. Diplomatarium

#### Status:
* Ikke startet
* TEI-XML filer fra Oslo og faksimiler fra NB, sy sammen. Potensielt samarbeidsprosjekt med Arkivverket og Språksamlingene.






